+++
title = "Recommendations"
+++

## Websites

- [based.cooking](https://based.cooking)

## Operating systems

- [NixOS](https://nixos.org/) with [my NixOS configuration](https://codeberg.org/papojari/nixos-config)
- [Arch Linux](https://archlinux.org/)

## Terminal emulators

- [alacritty](https://github.com/alacritty/alacritty)

## Xorg window managers/wayland compositors

- [sway](https://github.com/swaywm/sway)
- [bspwm](https://github.com/baskerville/bspwm)
- [dwm](https://dwm.suckless.org/)
