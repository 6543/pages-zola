+++
title = "Linux journey"
+++

My personal timeline of Linux distributions that I've used.

| Date installed | Distribution                               |
| -------------- | ------------------------------------------ |
| ≅2020/05       | [PopOS!](https://pop.system76.com/)        |
| ≅2020/06       | [Linux Mint](https://linuxmint.com/)       |
| ≅2020/07       | [Manjaro](https://manjaro.org/)            |
| ≅2020/09       | [Arch Linux](https://archlinux.org/)       |
| 2021/04/28     | [Bedrock Linux](https://bedrocklinux.org/) |
| ≅2021/05       | [Arch Linux](https://archlinux.org/)       |
| 2021/05/24     | [NixOS](https://nixos.org/)                |
