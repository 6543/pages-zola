+++
title = "Finance is weird"
date = 2021-06-24 00:08:25

[taxonomies]
authors = ["papojari"]
+++

## Credit cards

One thing I don't get is credit cards. Like why would I borrow money from a bank if I have money myself? Even if I don't have money I could just get a regular credit. Maybe because banks are giving neat premiums to credit card people? That's pretty evil. They're locking you into spending money you don't have.

Also, (*I'm kind of conflicted on this one*) how is interest on credit allowed? It's like letting a bank owe you money you didn't take from them. Someone said to me that interest on credit exists because

> Who pays the guy for his time ?

Another reason for interest would be a higher incentive to pay back I guess.

## VISA, Mastercard, … and why cryptocurrencies are superior

Why would you trust these companies with processing your payments? What if their services are offline? They don't open source their server software because anyone could just copy them I guess. But how do you know what is running on the servers? It is like a black box. I'd much rather like to pay with a hardware or software crypto wallet. If I choose the right **stable** cryptocurrency I can be anonymous and sure that no single entity has control over all of the worlds finance because cryptocurrencies are mostly decentralized, meaning the computers processing payments aren't in a single location owned by a single entity. With crypto I also know that for cryptocurrencies to be decentral their code has to be free so anyone can run a server.

Unfortunately cryptocurrencies are kind of a bandate at the moment because you need websites like BlockFi or Coinbase to exchange USD for example into a cryptocurrency.
